package com.example.worktime.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.worktime.R;
import com.example.worktime.model.AppDatabase;
import com.example.worktime.model.Project;
import com.example.worktime.model.ProjectDAO;
import com.example.worktime.model.SessionDAO;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class AddProjectFrag extends androidx.fragment.app.Fragment {
   private Button mButton;
   private Button cButton;
  private   EditText mEditDescription;
   private EditText mTextTitle;
   private ProjectDAO projectDAO;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {
            Context appCtx = getActivity().getApplicationContext();
            AppDatabase db = AppDatabase.getInstance(appCtx);
            SessionDAO sessionDAO = db.getSessionDAO();
            projectDAO = db.getProjectDAO();
            super.onCreateView(inflater, container, savedInstState);


            // Load the xml file that corresponds to this Java file.
            view = inflater.inflate(R.layout.frag_add_project, container, false);

            mButton = view.findViewById(R.id.btnAdd);
            mButton.setOnClickListener(new AddButtonClick());
            cButton = view.findViewById(R.id.btnCancel);
            cButton.setOnClickListener(new CancelButtonClick());


            // Get the floating action button and add a function to it.
           mEditDescription   =  view.findViewById(R.id.multiline);
            mTextTitle = view.findViewById(R.id.editTitle);

        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }


        return view;


    }
    private class AddButtonClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {
                mButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Project pi = new Project(mTextTitle.getText().toString(), mEditDescription.getText().toString());
                        projectDAO.insert(pi);
                        getActivity().onBackPressed();

                    }
                });
//
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
        }


    }
    private class CancelButtonClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {
                cButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        getActivity().onBackPressed();

                    }
                });
//
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
        }
    }
}
