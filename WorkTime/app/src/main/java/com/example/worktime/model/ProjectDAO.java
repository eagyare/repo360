package com.example.worktime.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


@Dao
public abstract class ProjectDAO {
	@Query("SELECT COUNT(*) FROM Project")
	public abstract long count();

	@Query("SELECT * FROM Project")
	public abstract List<Project> getAll();

	@Query("SELECT * FROM Project WHERE `key` = :key")
	public abstract Project getByKey(long key);

	public long insert(Project project) {
		long id = insertH(project);
		project.setKey(id);
		return id;
	}

	@Insert
	abstract long insertH(Project project);

	@Update
	public abstract void update(Project project);

	@Delete
	public abstract void delete(Project project);

	@Query("DELETE FROM Project")
	public abstract void deleteAll();
}
