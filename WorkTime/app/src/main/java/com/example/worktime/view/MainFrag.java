package com.example.worktime.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.worktime.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public final class MainFrag extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);

            // Load the xml file that corresponds to this Java file.
            view = inflater.inflate(R.layout.main_frag, container, false);

            RecyclerView recyclerView = view.findViewById(R.id.my_recycler_view);

            // use a linear layout manager
           RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
            recyclerView.setLayoutManager(layoutManager);

            // specify an adapter (see also next example)
            ProjectAdapter projectAdapter = new ProjectAdapter((MainActivity) getActivity());
            recyclerView.setAdapter(projectAdapter);
            // Get the floating action button and add a function to it.

            FloatingActionButton fav =   view.findViewById(R.id.floatingActionButton);
            fav.setOnClickListener(new ButtonClick());
        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }




    private class ButtonClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {
            Fragment frag = new AddProjectFrag();
            MainActivity acts = (MainActivity)getActivity();
            FragmentTransaction trans =
                    acts.getSupportFragmentManager().beginTransaction();
            trans.replace(R.id.fragContainer, frag);
            trans.addToBackStack(null);
            trans.commit();
//
        }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
        }
    }

}
