package com.example.worktime.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.Objects;

import static androidx.room.ForeignKey.CASCADE;


@Entity

public class Project {
    @PrimaryKey(autoGenerate = true)
    private long key;
    private String title;
    private String description;



    public Project(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public long getKey() {
        return key;
    }


    public String getDescription(){
        return description;
    }

    public void setKey(long key) {
        this.key = key;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals (Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Project project = (Project) other;
        return key == project.key &&
                title.equals(project.title) &&
                description.equals(project.description);
    }


    @Override
    public int hashCode() {
        return Objects.hash(key, title, description);
    }

    @Override
    public String toString() {
        return "Project{" +
                "key=" + key +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
