package com.example.worktime.model;

import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Project.class,
        parentColumns = "key",
        childColumns = "projectKey",
        onDelete = CASCADE
))

    public class Session {
        @PrimaryKey(autoGenerate = true)
        private long key;
        private long projectKey;
        private String description;
        private Date start;
        private Date end;


        @Ignore
        public Session(long key) {this.key = key;}

        @Ignore
        public Session(String description, Date start, Date end) {
            this.description = description;
            this.start = start;
            this.end = end;
        }

        public Session(long projectKey, String description, Date start, Date end) {
            this.projectKey = projectKey;
            this.description = description;
            this.start = start;
            this.end = end;
        }

        public long getKey() {
            return key;
        }

        public long getProjectKey() {
            return projectKey;
        }

        public String getDescription() {
            return description;
        }

        public Date getStart() {
            return start;
        }

        public Date getEnd() {
            return end;
        }

        public void setKey(long key) {
            this.key = key;
        }

        public void setProjectKey(long projectKey) {
            this.projectKey = projectKey;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setStart(Date start) {
            this.start = start;
        }

        public void setEnd(Date end) {
            this.end = end;
        }

        @Override
        public String toString() {
            return "Session{" +
                    "key=" + key +
                    ", projectKey=" + projectKey +
                    ", description='" + description + '\'' +
                    ", start=" + start +
                    ", end=" + end +
                    '}';
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Session)) return false;
            Session session = (Session) o;
            return getKey() == session.getKey() &&
                    getProjectKey() == session.getProjectKey() &&
                    getDescription().equals(session.getDescription()) &&
                    getStart().equals(session.getStart()) &&
                    getEnd().equals(session.getEnd());
        }

    }

