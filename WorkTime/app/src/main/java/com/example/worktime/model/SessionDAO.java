package com.example.worktime.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


@Dao
public abstract class SessionDAO {
	@Query("SELECT COUNT(*) FROM Session")
	public abstract long count();

	@Query("SELECT * FROM Session")
	public abstract List<Session> getAll();

	@Query("SELECT * FROM Session WHERE `key` = :key")
	public abstract Session getByKey(long key);

	public long insert(Session Session) {
		long id = insertH(Session);
		Session.setKey(id);
		return id;
	}

	@Insert
	abstract long insertH(Session session);

	@Update
	public abstract void update(Session session);

	@Delete
	public abstract void delete(Session session);

	@Query("DELETE FROM Session")
	public abstract void deleteAll();
}
