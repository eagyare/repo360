package com.example.worktime.model;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class ProjectTest {

    @Test
    public void testsession() {
        Context appCtx = InstrumentationRegistry.getInstrumentation().getTargetContext();
        AppDatabase db = AppDatabase.getInstance(appCtx);
        SessionDAO sessionDAO = db.getSessionDAO();
        ProjectDAO projectDAO = db.getProjectDAO();


        //Test both the Project and Session tables
        //For each table, write a test like this:
        //delete all rows from a table
        //verify that the count of that table is 0
        sessionDAO.deleteAll();
        assertEquals(0, sessionDAO.count());

        //insert two rows into that table
        Project pi = new Project("Fix Car", "Replace Starter");
        projectDAO.insert(pi);
        Project pr = new Project("Fix Moto", "Replace Starter");
        projectDAO.insert(pr);

        Session sr = new Session(pi.getKey(), "Replace Starter", new Date("12/12/2020"), new Date("12/12/2020"));
        Session se = new Session(pr.getKey(), "Replace Starter", new Date("12/12/2020"), new Date("12/12/2020"));
        sessionDAO.insert(sr);
        sessionDAO.insert(se);

        //verify that the count is 2
        assertEquals(2, sessionDAO.count());

        //retrieve the inserted rows
        Session s1 = sessionDAO.getByKey(sr.getKey());
        Session s2 = sessionDAO.getByKey(se.getKey());


        //verify that the data in the retrieved rows is exactly the same as the data that was inserted into the rows

        assertEquals(sr, s1);
        assertEquals(se, s2);


        //update one of the inserted rows
        sr.setDescription("Update");
        sessionDAO.update(sr);

        //verify that the count is still 2
        assertEquals(2, sessionDAO.count());

        //retrieve the updated row
        sr.getDescription();
        //verify that the data in the retrieved row is exactly the same as the data that was updated into the row

        assertEquals(sr.getDescription(), "Update");

        //retrieve the row that was not updated

        sr.getEnd();
        sr.getStart();
        //verify that the data in the non-updated row is correct
        assertEquals(sr.getEnd(), new Date("12/12/2020"));
        assertEquals(sr.getStart(), new Date("12/12/2020"));


        //delete one of the rows
        sessionDAO.delete(sr);

        //verify that the count is 1
        assertEquals(1, sessionDAO.count());

        //retrieve the row that is still in the database
        Session s3 = sessionDAO.getByKey(se.getKey());

        //verify that the remaining row contains the correct data
        assertEquals(se, s3);

        //delete the remaining row
        sessionDAO.delete(se);

        //verify that the count is 0
        assertEquals(0, sessionDAO.count());

    }
}